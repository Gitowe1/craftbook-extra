/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.data;

import java.util.List;
import java.util.Optional;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.Value;

import com.flowpowered.math.vector.Vector3d;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;

public class ChairExitLocationData extends AbstractSingleData<Vector3d, ChairExitLocationData, ImmutableChairExitLocationData>
{
    public ChairExitLocationData(List<Float> values)
    {
        this(new Vector3d(values.get(0), values.get(1), values.get(2)));
    }

	public ChairExitLocationData(Vector3d value)
	{
        super(CraftBookKeys.CHAIR_EXIT_LOCATION, value, new Vector3d(0, 0, 0));
    }

    @Override
    public Optional<ChairExitLocationData> fill(DataHolder dataHolder, MergeFunction overlap) {
        dataHolder.get(ChairExitLocationData.class).ifPresent((data) -> {
            ChairExitLocationData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<ChairExitLocationData> from(DataContainer container) {
        return container.getObject(CraftBookKeys.CHAIR_EXIT_LOCATION.getQuery(), Vector3d.class).map(ChairExitLocationData::new);
    }

    @Override
    public ChairExitLocationData copy() {
        return new ChairExitLocationData(this.getValue());
    }

    @Override
    public ImmutableChairExitLocationData asImmutable() {
        return new ImmutableChairExitLocationData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }

    @Override
    protected Value<Vector3d> getValueGetter()
    {
        return Sponge.getRegistry().getValueFactory().createValue(CraftBookKeys.CHAIR_EXIT_LOCATION, getValue());
    }
}
