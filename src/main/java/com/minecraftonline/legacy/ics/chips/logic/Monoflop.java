/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.ics.chips.logic;

import com.minecraftonline.util.ParsingConfiguration;
import com.sk89q.craftbook.sponge.ICFactory;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.minecraftonline.ic.AbstractStIC;
import com.sk89q.craftbook.sponge.mode.Modes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

//TODO: Remove later


/**
 * MONOFLOP
 * ModelID: MCU440
 *
 * @author Brendan  (doublehelix457)
 */
public class Monoflop extends AbstractStIC {

    public Monoflop(ICFactory<Monoflop> icFactory, Location<World> block){super(icFactory,block);}

    private final String TITLE = "MONOFLOP";
    // Prefix for when the monoflop is ready to begin counting, just needs a redstone signal
    private final String READY_PREFIX = "^";
    // Prefix for when the monoflop has been activated and is running.
    private final String RUNNING_PREFIX = "%";
    private Text lineOnLoad;
    private boolean setLine = false;

    // Amount of count.
    private int countLength;
    // How long to stay on for once count is complete.
    // If 0 then stay on forever.
    private int onLength;
    // How often to count.
    private int clockRate;
    // The current count.
    private int curCount;
    // The current progress towards incrementing the count.
    private int curClockProgress;

    // Once it has started we don't stop even if we lose power.
    private boolean started = false;

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);
        if (lines.get(2).toPlain().length() == 0)
        {
            throw new InvalidICException( "Specify counter configuration on line 3.");
        }

        String[] param = lines.get(2).toPlain().split(":", 3);

        this.setMode();

        int countLength;
        try {
            countLength = Integer.parseInt(param[0]);
        } catch(NumberFormatException e) {
            throw new InvalidICException("Count length is not a number");
        }

        int clockRate = 5;
        if (param.length > 1) {
            try {
                clockRate = Integer.parseInt(param[1]);
            } catch(NumberFormatException e) {
                throw new InvalidICException("Clock rate is not a number.");
            }
        }

        int onLength = getMode().getType() == Modes.CYCLE_OFF ? 1 : 0;
        if(param.length > 2) {
            try {
                onLength = Integer.parseInt(param[2]);
            } catch(NumberFormatException e) {
                throw new InvalidICException("On-count value is not a number.");
            }

            if(countLength < 1 || countLength > 999) {
                throw new InvalidICException("Count value must be a number from 1 to 999.");
            }

            if(onLength < 1 || onLength > 999) {
                throw new InvalidICException( "On-count value must be a number from 1 to 999.");
            }
        } else {
            if(countLength < 1 || countLength > 99999) {
                throw new InvalidICException("Count value must be a number from 1 to 99999.");
            }
        }

        if(clockRate < 5 || clockRate > 15) {
            throw new InvalidICException("Clock rate must be a number from 5 to 15.");
        }

        if (lines.get(3).toPlain().length() != 0) {
            throw new InvalidICException("The fourth line must be empty.");
        }

        if(param.length > 2) {
            lineOnLoad = Text.of(countLength + ":" + clockRate + ":" + onLength + ":" + countLength);
        } else {
            lineOnLoad = Text.of(countLength + ":" + clockRate + ":" + countLength);
        }
        setLine = true;
    }

    @Override
    public void load() {
        super.load();
        if(setLine)
            setLine(2, lineOnLoad);

        setMode();
        String[] splitline = getLine(2).split(":");
        if (splitline.length == 4) {
            countLength = Integer.parseInt(splitline[0]);
            clockRate = Integer.parseInt(splitline[1]);
            onLength = Integer.parseInt(splitline[2]);
            curCount = Integer.parseInt(splitline[3]);
        }
        else {
            countLength = Integer.parseInt(splitline[0]);
            clockRate = Integer.parseInt(splitline[1]);
            curCount = Integer.parseInt(splitline[2]);
            if (getMode().getType() == Modes.CYCLE_OFF)
                onLength = 1;
        }
        curClockProgress = getLine(3).length();
        started = getLine(0).startsWith(RUNNING_PREFIX);
    }

    @Override
    public void unload() {
        if (onLength != 0 && getMode().getType() != Modes.CYCLE_OFF)
            setLine(2, Text.of(countLength + ":" + clockRate + ":" + onLength + ":" + curCount));
        else
            setLine(2, Text.of(countLength + ":" + clockRate + ":" + curCount));
        StringBuilder spaces = new StringBuilder();
        for (int i = 0; i < curClockProgress; i++) {
            spaces.append(" ");
        }
        setLine(0, Text.of((started ? RUNNING_PREFIX : READY_PREFIX) + TITLE));
        setLine(3, Text.of(spaces.toString()));
    }

    //TODO This needs heavy cleanup and work. New mode system isnt very helpful on a per-IC basis.
    @Override
    public void onTrigger() {
        if (!started && getPinSet().isAnyTriggered(this)) {
            getPinSet().setOutput(0, false, this);
            started = true;
            curCount = countLength;
            setLine(0, Text.of(RUNNING_PREFIX + TITLE));
        }
    }

    @Override
    public void think() {
        if (!started) {
            return;
        }
        if (curClockProgress >= clockRate) {
            curCount--;
            curClockProgress = 0;
        } else {
            curClockProgress++;
        }

        if (curCount == 0) {
            // Hit end of countdown - turn on.
            getPinSet().setOutput(0, true, this);
            if (onLength == 0) {
                // If not staying on for a set period of time, stay on forever:
                reset();
            }
        }
        else if (curCount <= 0) {
            if (-curCount >= onLength) {
                reset();
                getPinSet().setOutput(0, false, this);
            }
        }
    }

    public void reset() {
        setLine(0, Text.of(READY_PREFIX + TITLE));
        started = false;
        curCount = 0;
    }

    public static class Factory implements ICFactory<Monoflop> {

        @Override
        public Monoflop createInstance(Location<World> location){return new Monoflop(this,location);}

        @Override
        public String[][] getPinHelp(){
            return new String[][] {
                    new String[] {
                            "Input",
                    },
                    new String[] {
                            "Output"
                    }
            };
        }
    }

    @Override
    public boolean isAlwaysST() {return true;}
}
