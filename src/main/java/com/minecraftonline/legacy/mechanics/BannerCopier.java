/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import net.minecraft.tileentity.TileEntityBanner;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Banner;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Optional;

import static org.spongepowered.common.item.inventory.util.ItemStackUtil.fromNative;

@Module(id = "bannercopier", name = "BannerCopier", onEnable="onInitialize", onDisable="onDisable")
public class BannerCopier extends SpongeSignMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.bannercopier", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    @Listener
    public void onBlockInteract(InteractBlockEvent.Secondary.MainHand event) {
        if (!(event.getSource() instanceof Player && event.getTargetBlock().getState().getType().equals(BlockTypes.BOOKSHELF) || event.getTargetBlock().getState().getType().equals(BlockTypes.WALL_SIGN)))
            return;

        if ((event.getSource() instanceof Player && event.getTargetBlock().getState().getType().equals(BlockTypes.WALL_SIGN))) {
            Sign wallSign = (Sign) event.getTargetBlock().getLocation().get().getTileEntity().orElse(null);
            if (wallSign == null || !(isMechanicSign(wallSign.getSignData()))) {
                return;
            }
        }

        Player player = ((Player) event.getSource());
        if (!player.getItemInHand(HandTypes.MAIN_HAND).isPresent())
            return;

        ItemStack helditem = player.getItemInHand(HandTypes.MAIN_HAND).get();
        if (!helditem.getType().equals(ItemTypes.BANNER)) {
            return;
        }
        event.getTargetBlock().getLocation().flatMap(this::getBannerCopierSign).ifPresent(sign -> {
            Optional<Banner> bannerOptional = getBannerNearSign(sign.getLocation());
            if (!bannerOptional.isPresent()) {
                player.sendMessage(Text.of("No banner above or below sign present to copy"));
                return;
            }
            TileEntityBanner bannerTileEntity = (TileEntityBanner) bannerOptional.get();
            event.setCancelled(true);

            // ItemStack to check the banner's data
            net.minecraft.item.ItemStack bannerItemStack = bannerTileEntity.getItem();

            // Check if the banner has more than 6 patterns
            int patternCount = TileEntityBanner.getPatterns(bannerItemStack);
            if (patternCount > 6) {
                player.sendMessage(Text.of("This banner has more than 6 patterns and cannot be copied."));
                return;
            }

            // ItemStack to actually copy the banner
            ItemStack newBannerItemStack = fromNative(bannerTileEntity.getItem());


            if (!player.getInventory().canFit(newBannerItemStack)) {
                player.sendMessage(Text.of("You cannot fit the new banner"));
                return;
            }
            helditem.setQuantity(helditem.getQuantity() - 1);
            player.getInventory().offer(newBannerItemStack);
        });
    }

    public Optional<Banner> getBannerNearSign(Location<World> signLocation) {
        Vector3i[] checkLocs = new Vector3i[] {
                new Vector3i(0, -1, 0), // Check one block below the sign
                new Vector3i(0, -2, 0), // Check two blocks below the sign
                new Vector3i(0, 1, 0), // Check one block above the sign
                new Vector3i(0, 2, 0), // Check two blocks above the sign
        };
        for (Vector3i checkLoc : checkLocs) {
            Optional<TileEntity> tileEntity = signLocation.add(checkLoc).getTileEntity();
            if (tileEntity.isPresent() && tileEntity.get() instanceof Banner) {
                return Optional.of((Banner) tileEntity.get());
            }
        }
        return Optional.empty();
    }

    public Optional<Sign> getBannerCopierSign(Location<World> loc) {
        if (loc.getBlockType().equals(BlockTypes.WALL_SIGN)) {
            Sign sign = (Sign) loc.getTileEntity().get();
            if (isMechanicSign(sign.getSignData()))
                return Optional.of(sign);
        }
        for (Vector3d dir : ADJACENTS) {
            if (loc.add(dir).getBlockType().equals(BlockTypes.WALL_SIGN)) {
                Sign sign = (Sign) loc.add(dir).getTileEntity().get();
                if (isMechanicSign(sign.getSignData()))
                    return Optional.of(sign);
            }
        }
        return Optional.empty();
    }

    private static final Vector3d[] ADJACENTS = new Vector3d[]{
            new Vector3d(1,0,0),
            new Vector3d(-1,0,0),
            new Vector3d(0,0,1),
            new Vector3d(0,0,-1)
    };

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        if (!location.getBlockType().equals(BlockTypes.WALL_SIGN)) {
            if (player != null)
                player.sendMessage(Text.of(TextColors.RED, "BannerCopier must be a wall sign!"));
            return false;
        }
        if (!location.add(location.require(Keys.DIRECTION).getOpposite().asBlockOffset()).getBlockType().equals(BlockTypes.BOOKSHELF)) {
            if (player != null)
                player.sendMessage(Text.of(TextColors.RED, "BannerCopier must be placed on a bookshelf!"));
            return false;
        }
        return true;
    }

    @Override
    public String[] getValidSigns() {
        return new String[] {"[BannerCopier]"};
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }
}
