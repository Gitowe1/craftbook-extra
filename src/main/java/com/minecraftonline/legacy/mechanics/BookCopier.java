/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mechanics;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeSignMechanic;
import com.sk89q.craftbook.sponge.util.SpongePermissionNode;
import net.minecraft.nbt.NBTTagCompound;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.hanging.ItemFrame;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.AABB;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.spongepowered.common.item.inventory.util.ItemStackUtil.toNative;

@Module(id = "bookcopier", name = "BookCopier", onEnable="onInitialize", onDisable="onDisable")
public class BookCopier extends SpongeSignMechanic {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final SpongePermissionNode createPermissions = new SpongePermissionNode("craftbook.bookcopier", "Allows the user to create the " + getName() + " mechanic.", PermissionDescription.ROLE_USER);

    @Listener
    public void onBlockInteract(InteractBlockEvent.Secondary.MainHand event) {
        if (!(event.getSource() instanceof Player && event.getTargetBlock().getState().getType().equals(BlockTypes.BOOKSHELF) || event.getTargetBlock().getState().getType().equals(BlockTypes.WALL_SIGN)))
            return;

        if ((event.getSource() instanceof Player && event.getTargetBlock().getState().getType().equals(BlockTypes.WALL_SIGN))) {
            Sign wallSign = (Sign) event.getTargetBlock().getLocation().get().getTileEntity().orElse(null);
            if (wallSign == null || !(isMechanicSign(wallSign.getSignData()))) {
                return;
            }
        }

        Player player = ((Player) event.getSource());

        BlockType targetBlockType = event.getTargetBlock().getState().getType();
        if (targetBlockType != BlockTypes.WALL_SIGN && targetBlockType != BlockTypes.BOOKSHELF) {
            return;
        }

        if (!player.getItemInHand(HandTypes.MAIN_HAND).isPresent())
            return;

        ItemStack heldItem = player.getItemInHand(HandTypes.MAIN_HAND).get();

        if (!heldItem.getType().equals(ItemTypes.WRITABLE_BOOK)) {
            return;
        }
        event.getTargetBlock().getLocation().flatMap(this::getBookCopierSign).ifPresent(sign -> {
            Optional<ItemFrame> itemFrameOptional = getItemFrameNearSign(sign.getLocation(), player);
            if (!itemFrameOptional.isPresent()) {
                return;
            }
            ItemFrame itemFrame = itemFrameOptional.get();
            ItemStackSnapshot itemInFrameSnapshot = itemFrame.get(Keys.REPRESENTED_ITEM).orElse(ItemStackSnapshot.NONE);
            ItemStack itemInFrame = itemInFrameSnapshot.createStack();

            if (itemInFrame.getType().equals(ItemTypes.WRITTEN_BOOK)) {
                event.setCancelled(true);

                // Extract generation, pages, author, and title from the book
                Optional<Integer> generationOpt = itemInFrame.get(Keys.GENERATION);
                Optional<List<Text>> pagesOpt = itemInFrame.get(Keys.BOOK_PAGES);
                Optional<Text> authorOpt = itemInFrame.get(Keys.BOOK_AUTHOR);
                Optional<Text> titleOpt = itemInFrame.get(Keys.DISPLAY_NAME);
                Optional<List<Text>> loreOpt = itemInFrame.get(Keys.ITEM_LORE);

                if (!generationOpt.isPresent() || !pagesOpt.isPresent() || !authorOpt.isPresent() || !titleOpt.isPresent()) {
                    player.sendMessage(Text.of("Invalid book data"));
                    return;
                }

                if (loreOpt.isPresent() && loreOpt.get().stream().anyMatch(lore -> lore.toPlain().equalsIgnoreCase("Reprint"))) {
                    player.sendMessage(Text.of("Reprints cannot be reprinted again!"));
                    return;
                }

                int generation = generationOpt.get();
                if (generation > 2) {
                    player.sendMessage(Text.of("This book is too tattered to be copied!"));
                    return;
                } else if (generation == 2) {
                    player.sendMessage(Text.of("Copies of copies cannot be reprinted!"));
                    return;
                } else if (generation == 0) {
                    // Ensures that original books always make copies when reprinted
                    generation++;
                }

                // Create a new book with the same data but updated generation
                ItemStack newBookItemStack = ItemStack.builder()
                        .itemType(ItemTypes.WRITTEN_BOOK)
                        .add(Keys.BOOK_PAGES, pagesOpt.get())
                        .add(Keys.BOOK_AUTHOR, authorOpt.get())
                        .add(Keys.DISPLAY_NAME, titleOpt.get())
                        .add(Keys.GENERATION, generation)
                        .add(Keys.ITEM_LORE, Collections.singletonList(Text.of(TextColors.GRAY, "Reprint")))
                        .build();

                if (!player.getInventory().canFit(newBookItemStack)) {
                    player.sendMessage(Text.of("Your hands are too full to carry the new book!"));
                    return;
                }

                heldItem.setQuantity(heldItem.getQuantity() - 1);
                player.getInventory().offer(newBookItemStack);
            } else {
                player.sendMessage(Text.of("No book is present to copy from in the item frame"));
            }
        });
    }

    public Optional<ItemFrame> getItemFrameNearSign(Location<World> signLocation, Player player) {
        Optional<AABB> aabb = getAABB(signLocation);
        if (!aabb.isPresent()) {
            return Optional.empty();
        }
        Collection<Entity> entities = signLocation.getExtent().getIntersectingEntities(aabb.get());

        List<ItemFrame> itemFrames = entities.stream()
                .filter(entity -> entity instanceof ItemFrame)
                .map(entity -> (ItemFrame) entity)
                .collect(Collectors.toList());

        // Check if there's exactly one item frame
        if (itemFrames.size() == 1) {
            return Optional.of(itemFrames.get(0));
        }

        if (itemFrames.isEmpty()) {
            player.sendMessage(Text.of("No item frame with book present to copy"));
        } else {
            player.sendMessage(Text.of("More than one item frame found within sign range"));
            player.sendMessage(Text.of("Ensure there is only one item frame above or below the sign"));
        }

        // Return empty if there are no item frames or more than one
        return Optional.empty();
    }

    public Optional<AABB> getAABB(Location<World> location) {
        Vector3i pos = location.getBlockPosition();
        Vector3i min = pos.add(0, -2, 0);
        Vector3i max = pos.add(1, 3, 1);
        return Optional.of(new AABB(min, max));
    }

    public Optional<Sign> getBookCopierSign(Location<World> loc) {
        if (loc.getBlockType().equals(BlockTypes.WALL_SIGN)) {
            Sign sign = (Sign) loc.getTileEntity().get();
            if (isMechanicSign(sign.getSignData()))
                return Optional.of(sign);
        }
        for (Vector3d dir : signAdjacents) {
            if (loc.add(dir).getBlockType().equals(BlockTypes.WALL_SIGN)) {
                Sign sign = (Sign) loc.add(dir).getTileEntity().get();
                if (isMechanicSign(sign.getSignData()))
                    return Optional.of(sign);
            }
        }
        return Optional.empty();
    }

    static Vector3d[] signAdjacents = new Vector3d[]{
            new Vector3d(1, 0, 0),
            new Vector3d(-1, 0, 0),
            new Vector3d(0, 0, 1),
            new Vector3d(0, 0, -1),
    };

    @Override
    public boolean verifyLines(Location<World> location, SignData signData, @Nullable Player player) {
        if (!location.getBlockType().equals(BlockTypes.WALL_SIGN)) {
            if (player != null)
                player.sendMessage(Text.of(TextColors.RED, "BookCopier must be a wall sign!"));
            return false;
        }
        if (!location.add(location.require(Keys.DIRECTION).getOpposite().asBlockOffset()).getBlockType().equals(BlockTypes.BOOKSHELF)) {
            if (player != null)
                player.sendMessage(Text.of(TextColors.RED, "BookCopier must be placed on a bookshelf!"));
            return false;
        }
        return true;
    }

    @Override
    public String[] getValidSigns() {
        return new String[]{"[BookCopier]"};
    }

    @Override
    public SpongePermissionNode getCreatePermission() {
        return createPermissions;
    }
}




