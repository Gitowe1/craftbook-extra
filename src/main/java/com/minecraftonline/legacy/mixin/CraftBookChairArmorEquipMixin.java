/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.util.EntitySelectors;

import javax.annotation.Nullable;

import org.spongepowered.api.entity.living.Living;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;

@Mixin(EntitySelectors.ArmoredMob.class)
public abstract class CraftBookChairArmorEquipMixin {

    @Inject(method = "apply", at = @At(value = "RETURN", ordinal = 4))
    public void onApply(@Nullable Entity p_apply_1_, CallbackInfoReturnable<Boolean> cir) {
        // don't consider chairs to be armor mobs
        // so dispensers do not equip armor onto them
        if (((Living) p_apply_1_).get(CraftBookKeys.CHAIR).orElse(false))
        {
            cir.setReturnValue(false);
        }
    }
}
