/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.legacy.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;

@Mixin(EntityLivingBase.class)
public abstract class CraftBookChairWaterDismountMixin extends Entity {

    public CraftBookChairWaterDismountMixin(World worldIn) {
        super(worldIn);
    }

    @Redirect(method = "onEntityUpdate", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/EntityLivingBase;dismountRidingEntity()V"))
    public void onDismountRidingEntityInWater(EntityLivingBase entity) {
        // don't dismount chairs when underwater
        if (!((Living) this.getRidingEntity()).get(CraftBookKeys.CHAIR).orElse(false))
        {
            entity.dismountRidingEntity();
        }
    }
}
