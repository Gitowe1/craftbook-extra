/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.me4502.modularframework.module.Module;
import com.me4502.modularframework.module.guice.ModuleConfiguration;
import com.minecraftonline.data.ChairData;
import com.minecraftonline.data.ChairExitLocationData;
import com.minecraftonline.data.ChairHealData;
import com.sk89q.craftbook.core.util.ConfigValue;
import com.sk89q.craftbook.core.util.CraftBookException;
import com.sk89q.craftbook.core.util.PermissionNode;
import com.sk89q.craftbook.core.util.documentation.DocumentationProvider;
import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.mechanics.types.SpongeBlockMechanic;
import com.sk89q.craftbook.sponge.util.*;
import com.sk89q.craftbook.sponge.util.data.CraftBookKeys;
import com.sk89q.craftbook.sponge.util.type.TypeTokens;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import ninja.leaping.configurate.ConfigurationNode;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.data.property.AbstractProperty;
import org.spongepowered.api.data.property.block.MatterProperty;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.data.type.PortionTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.entity.RideEntityEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.type.Include;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.service.permission.SubjectData;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.Tristate;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.*;
import java.util.Map.Entry;

/**
 * A significant amount of re-doing from original craftbook chairs (didnt work properly before)
 * plus added healing chairs
 */
@Module(id = "chairs", name = "Chairs", onEnable = "onInitialize", onDisable = "onDisable")
public class Chairs extends SpongeBlockMechanic implements DocumentationProvider {

    @Inject
    @ModuleConfiguration
    public ConfigurationNode config;

    private final ConfigValue<List<SpongeBlockFilter>> allowedBlocks = new ConfigValue<>("allowed-blocks", "A list of blocks that can be used.", getDefaultBlocks(), new TypeTokens.BlockFilterListTypeToken());
    private final ConfigValue<Boolean> exitAtEntry = new ConfigValue<>("exit-at-last-position", "Moves player's to their entry position when they exit the chair.", false);
    private final ConfigValue<Boolean> requireSigns = new ConfigValue<>("require-sign", "Require signs on the chairs.", false);
    private final ConfigValue<Integer> maxSignDistance = new ConfigValue<>("max-sign-distance", "The distance the sign can be from the clicked chair.", 3);
    private final ConfigValue<Boolean> faceCorrectDirection = new ConfigValue<>("face-correct-direction", "When the player sits, automatically face them the direction of the chair. (If possible)", true);
    private final ConfigValue<Double> healAmount = new ConfigValue<>("heal-amount", "Amount to heal the player by.", 1.0d, TypeToken.of(Double.class));
    private final ConfigValue<Integer> healRate = new ConfigValue<>("heal-rate", "How often to heal the player", 10);

    private final SpongePermissionNode useSitCommandPermission = new SpongePermissionNode("craftbook.chairs.use.sit", "Allows the user to sit in chairs using /sit", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode toggleClickSitPermission = new SpongePermissionNode("craftbook.chairs.use.toggle", "Allows the user to sit in chairs using /sit", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode clickSitPermission = new SpongePermissionNode("craftbook.chairs.use.click", "Allows the user to sit in chairs by clicking", PermissionDescription.ROLE_USER);
    private final SpongePermissionNode createSitHealPermission = new SpongePermissionNode("craftbook.chairs.createhealchair", "Allows the user to sit in chairs.", PermissionDescription.ROLE_USER);

    private CommandMapping sitCommandMapping;
    private CommandMapping standCommandMapping;
    private CommandMapping toggleClickSitCommandMapping;
    private List<CustomChairType> customChairTypes;

    /**
     * A set of UUIDs belonging to players in healing chairs .
     */
    private Set<UUID> chairHealingPlayers;

    /**
     * A map of locations to chair entity UUIDs
     */
    private Map<UUID, Location<World>> chairLocations;

    /**
     * A map of player UUIDs and when they last used /sit or dismounted a chair (in seconds since epoch)
     */
    private Map<UUID, Long> cooldowns;

    @Override
    public void onInitialize() throws CraftBookException {
        super.onInitialize();

        customChairTypes = new ArrayList<>();
        chairHealingPlayers = new HashSet<>();
        chairLocations = new HashMap<>();
        cooldowns = new HashMap<>();

        allowedBlocks.load(config);
        exitAtEntry.load(config);
        requireSigns.load(config);
        maxSignDistance.load(config);
        faceCorrectDirection.load(config);
        healAmount.load(config);
        healRate.load(config);

        useSitCommandPermission.register();
        clickSitPermission.register();
        toggleClickSitPermission.register();
        createSitHealPermission.register();

        // Register all custom chairs
        customChairTypes.add(new HealingChairType(this, createSitHealPermission));

        CommandSpec sit = CommandSpec.builder()
                .permission(useSitCommandPermission.getNode())
                .executor(new Sit(this))
                .build();

        sitCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), sit, "sit").orElse(null);

        CommandSpec stand = CommandSpec.builder()
                .permission(useSitCommandPermission.getNode())
                .executor(new Stand(this))
                .build();

        standCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(), stand, "stand").orElse(null);

        toggleClickSitCommandMapping = Sponge.getCommandManager().register(CraftBookPlugin.spongeInst(),
                CommandSpec.builder()
                        .permission(this.toggleClickSitPermission.getNode())
                        .executor(new ToggleClickSit(this))
                        .build(), "sittoggle")
                .orElse(null);

        Sponge.getGame().getScheduler().createTaskBuilder().intervalTicks(healRate.getValue()).execute(task -> {
            for (Iterator<UUID> uuid = chairHealingPlayers.iterator(); uuid.hasNext();) {
                Optional<Player> optPlayer = Sponge.getGame().getServer().getPlayer(uuid.next());
                if (!optPlayer.isPresent()) {
                    // player is gone
                    uuid.remove();
                    continue;
                }
                Player player = optPlayer.get();
                Optional<Entity> chair = player.getVehicle();
                if (!chair.isPresent() || !chair.get().get(CraftBookKeys.CHAIR).orElse(false)
                    || !chair.get().get(CraftBookKeys.HEALING_CHAIR).orElse(false)) {
                    // player is no longer on a healing chair
                    uuid.remove();
                    continue;
                }
                if (player.get(Keys.HEALTH).orElse(0d) < player.get(Keys.MAX_HEALTH).orElse(0d)) {
                    player.offer(Keys.HEALTH, Math.min(player.get(Keys.HEALTH).orElse(0d) + healAmount.getValue(), player.get(Keys.MAX_HEALTH).orElse(0d)));
                }

                if (player.get(Keys.EXHAUSTION).orElse(-20d) > -20d) {
                    player.offer(Keys.EXHAUSTION, player.get(Keys.EXHAUSTION).orElse(-20d) - 0.1d);
                }
            }
        }).submit(CraftBookPlugin.inst());
    }

    @Override
    public void onDisable() {
        super.onDisable();

        if (sitCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(sitCommandMapping);
        }
        if (standCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(standCommandMapping);
        }
        if (toggleClickSitCommandMapping != null) {
            Sponge.getCommandManager().removeMapping(toggleClickSitCommandMapping);
        }
        for (Iterator<Entry<UUID, Location<World>>> iterator = chairLocations.entrySet().iterator(); iterator.hasNext();) {
            Entry<UUID, Location<World>> chairLocation = iterator.next();
            // don't remove from the collection via removeChair while iterating over it
            chairLocation.getValue().getExtent().getEntity(chairLocation.getKey()).ifPresent(chair -> removeChair(chair, true, false));
            iterator.remove();
        }

        chairHealingPlayers.clear();
        customChairTypes.clear();
        cooldowns.clear();
    }

    @Override
    public boolean isValid(Location<World> location) {
        return isValid(location, true);
    }

    public boolean isValid(Location<World> location, boolean checkBlock) {
        // allow sitting underwater
        // TODO: support sitting in chairs with blocks that have no hitbox above them (eg string, torch)
        return (!checkBlock || isAllowed(location.getBlock())) &&
                (location.getBlockY() == 0 || checkMatter(location.getRelative(Direction.DOWN), MatterProperty.Matter.SOLID)) &&
                !checkMatter(location.getRelative(Direction.UP), MatterProperty.Matter.SOLID);
    }

    private Optional<Sign> getSign(Location<World> location, List<Location<World>> searched, Location<World> original) {

        for (Direction face : BlockUtil.getDirectHorizontalFaces()) {
            Location<World> otherBlock = location.getRelative(face);

            if (searched.contains(otherBlock)) continue;
            searched.add(otherBlock);

            if (location.getPosition().distanceSquared(original.getPosition()) > Math.pow(maxSignDistance.getValue(), 2)) continue;

            if (SignUtil.isSign(otherBlock) && SignUtil.getFront(otherBlock) == face) {
                return Optional.of((Sign)otherBlock.getTileEntity().get());
            }

            if (Objects.equals(location.getBlockType(), otherBlock.getBlockType())) {
                return getSign(otherBlock, searched, original);
            }
        }

        return Optional.empty();
    }

    private void addChair(Player player, @Nullable SignData signData, Location<World> location) {
        Location<World> chairLocation = location.getExtent().getLocation(location.getBlockPosition().toDouble().add(0.5, 0.3, 0.5));
        Entity chair = createChair(chairLocation, player.getLocation());

        if (faceCorrectDirection.getValue() && location.supports(Keys.DIRECTION)) {
            Vector3d euler = LocationUtil.cartesianToEuler(location.get(Keys.DIRECTION).orElse(Direction.NONE).getOpposite().asOffset());
            chair.setRotation(euler);
            player.setRotation(euler);
        }

        Sponge.getCauseStackManager().pushCause(player);
        if (!location.getExtent().spawnEntity(chair)) {
            player.sendMessage(Text.of(TextColors.RED, "Failed to create a sitable seat for you"));
            return;
        }
        Sponge.getCauseStackManager().popCause();

        if (signData != null) {
            for (CustomChairType chairType : customChairTypes) {
                if (chairType.matches(signData)) {
                    chairType.processChair(chair);
                    chairType.processPlayerSit(player);
                    break;
                }
            }
        }

        if (!chair.addPassenger(player) /*|| !entity.hasPassenger(player)*/) {
            chair.remove();
            player.sendMessage(Text.of("Failed to sit you down."));
            return;
        }
        player.sendMessage(Text.of(TextColors.YELLOW, "You sit down!"));

        cooldowns.put(player.getUniqueId(), System.currentTimeMillis() / 1000L);

        chairLocations.put(chair.getUniqueId(), chairLocation);
    }

    private void removeChair(Entity chair, boolean clearPassengers) {
        removeChair(chair, clearPassengers, true);
    }

    private void removeChair(Entity chair, boolean clearPassengers, boolean removeFromCollection) {
        if (clearPassengers) {
            Optional<Location<World>> exitLocation;
            if (exitAtEntry.getValue()) {
                exitLocation = chair.get(CraftBookKeys.CHAIR_EXIT_LOCATION).map(
                        value -> new Location<World>(chair.getWorld(), value));
            } else {
                exitLocation = Optional.empty();
            }
            chair.getPassengers().stream()
                    .filter(entity -> entity instanceof Player)
                    .map(p -> (Player) p)
                    .forEach(p -> {
                        Location<World> originalLocation = p.getLocation();
                        p.setVehicle(null);
                        Sponge.getScheduler().createTaskBuilder().delayTicks(5L).execute(() -> {
                            if (exitLocation.isPresent()) {
                                p.setLocation(exitLocation.get());
                            } else {
                                dismountToSafeLocation(p, originalLocation);
                            }
                        }).submit(CraftBookPlugin.inst());
                    });
            chair.clearPassengers();
        }

        chair.remove();
        if (removeFromCollection) {
            chairLocations.remove(chair.getUniqueId());
        }
    }

    /**
     * Use bed tactics to safely put them somewhere,
     * or murder them and stick them above if theres
     * nowhere to put em.
     *
     * @param player Player to safely dismount.
     */
    private void dismountToSafeLocation(Player player, Location<World> originalPosition) {
        Location<World> loc = originalPosition;
        Vector3d pos = Vector3d.from(loc.getX(), loc.getBlockY(), loc.getZ());
        loc = loc.setPosition(pos);

        int[] priorityArray = new int[]{0, -1, 1};

        for (int x : priorityArray) {
            for (int y : priorityArray) {
                for (int z : priorityArray) {
                    Location<World> testLoc = loc.add(x, y, z);
                    if (checkMatter(testLoc, MatterProperty.Matter.SOLID)
                            && checkMatter(testLoc.add(0, 1, 0), MatterProperty.Matter.GAS)
                            && checkMatter(testLoc.add(0, 2, 0), MatterProperty.Matter.GAS)
                    ) {
                        player.setLocation(getTopOfBlock(testLoc));
                        //loc.get(Keys.DIRECTION).ifPresent(dir -> player.setRotation(LocationUtil.cartesianToEuler(dir.getOpposite().asOffset())));
                        return;
                    }
                }
            }
        }
        player.setLocation(player.getLocation().add(0, 1, 0));
    }

    public static Optional<MatterProperty.Matter> getMatter(Location<World> loc) {
        return loc.getBlockType().getProperty(MatterProperty.class).map(AbstractProperty::getValue);
    }

    public static boolean checkMatter(Location<World> loc, MatterProperty.Matter matter) {
        return getMatter(loc).filter(m -> m.equals(matter)).isPresent();
    }

    public static Location<World> getTopOfBlock(Location<World> loc) {
        BlockPos pos = new BlockPos(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        Vec3d end = new Vec3d(loc.getX(), loc.getBlockY(), loc.getZ());
        Vec3d start = end.add(0, 1.1, 0);

        RayTraceResult result = ((IBlockState) loc.getBlock()).collisionRayTrace((net.minecraft.world.World) loc.getExtent(), pos, start, end);
        if (result != null) {
            return loc.setPosition(Vector3d.from(result.hitVec.x, result.hitVec.y, result.hitVec.z));
        }
        return loc;
    }

    /**
     * Gets the chair by the chair's block position
     *
     * @param location The location of the chair entity.
     * @return The chair, or null if no chair had the given uuid.
     */
    private Optional<Entity> getChairAtBlock(Location<World> location) {
        return chairLocations.entrySet().stream()
                .filter(entry -> entry.getValue().getExtent().equals(location.getExtent()))
                .filter(entry -> entry.getValue().getBlockPosition().equals(location.getBlockPosition()))
                .findAny()
                .flatMap(entry -> entry.getValue().getExtent().getEntity(entry.getKey()));
    }

    @Listener
    public void onBlockClick(InteractBlockEvent.Secondary.MainHand event, @First Player player) {
        event.getTargetBlock().getLocation().ifPresent(location -> {
            if (!isValid(location))
                return;

            if (player.getItemInHand(HandTypes.MAIN_HAND).filter(item -> item.getType() != ItemTypes.AIR).isPresent()) {
                return;
            }

            if (player.get(Keys.IS_SNEAKING).orElse(false))
                return;

            if (!clickSitPermission.hasPermission(player)) {
                player.sendMessage(Text.of(TextColors.RED, "You don't have permission to click-sit. Maybe try re-enabling with /sittoggle"));
                return;
            }
            Optional<Sign> optSign = getSign(location, new ArrayList<>(), location);
            if (requireSigns.getValue() && !optSign.isPresent()) {
                return;
            }

            if (event.getTargetSide() == Direction.DOWN
                    || (event.getTargetBlock().supports(Keys.DIRECTION) && event.getTargetSide() == event.getTargetBlock().require(Keys.DIRECTION))) {
                player.sendMessage(Text.of(TextColors.RED, "You cannot reach the chair from below/behind"));
                return;
            }

            if (getChairAtBlock(location).isPresent()) {
                player.sendMessage(Text.of(TextColors.RED, "Chair already occupied!"));
                return;
            }

            Optional<Entity> vehicle = player.getVehicle();
            if (vehicle.isPresent() && vehicle.get().get(CraftBookKeys.CHAIR).orElse(false)) {
                removeChair(vehicle.get(), true);
            }

            addChair(player, optSign.map(Sign::getSignData).orElse(null), location);
        });
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break event) {
        event.getTransactions().forEach((transaction) -> transaction.getOriginal().getLocation().ifPresent((location) -> {
            Optional<Entity> chair = getChairAtBlock(location);
            if (!chair.isPresent()) {
                chair = getChairAtBlock(location.add(0, 1, 0));
            }
            if (chair.isPresent()) {
                removeChair(chair.get(), true);
            }
        }));
    }

    @Listener
    public void onDismount(RideEntityEvent.Dismount event, @First Player player) {
        if (!event.getTargetEntity().get(CraftBookKeys.CHAIR).isPresent()) return;
        cooldowns.put(player.getUniqueId(), System.currentTimeMillis() / 1000L);
        Entity chair = event.getTargetEntity();
        Location<World> originalLocation = player.getLocation().add(0, 1, 0);
        player.sendMessage(Text.of(TextColors.YELLOW, "You stand up!"));
        Sponge.getScheduler().createTaskBuilder().execute(() -> {
            if (exitAtEntry.getValue()) {
                Optional<Location<World>> exitLocation = chair.get(CraftBookKeys.CHAIR_EXIT_LOCATION).map(
                        value -> new Location<World>(chair.getWorld(), value));
                if (exitLocation.isPresent()) {
                    player.setLocation(exitLocation.get());
                } else {
                    dismountToSafeLocation(player, originalLocation);
                }
            } else {
                dismountToSafeLocation(player, originalLocation);
            }
        }).submit(CraftBookPlugin.inst());
        if (chair.getPassengers().size() - 1 <= 0) {
            removeChair(chair, false);
        }
    }

    @Listener
    public void onLogout(ClientConnectionEvent.Disconnect event) {
        Optional<Entity> chair = event.getTargetEntity().getVehicle();
        if (chair.isPresent() && chair.get().get(CraftBookKeys.CHAIR).orElse(false)) {
            // remove this chair from currently used locations, as it is taken with the player on logout
            chairLocations.remove(chair.get().getUniqueId());
        }
        chairHealingPlayers.remove(event.getTargetEntity().getUniqueId());
        cooldowns.remove(event.getTargetEntity().getUniqueId());
        // don't remove the actual chair entity here, it is taken with the player when they logout
    }

    @Listener
    public void onLogin(ClientConnectionEvent.Join event) {
        Optional<Entity> chair = event.getTargetEntity().getVehicle();
        if (chair.isPresent() && chair.get().get(CraftBookKeys.CHAIR).orElse(false)) {
            // player has logged in already sitting on a chair
            cooldowns.put(event.getTargetEntity().getUniqueId(), System.currentTimeMillis() / 1000L);
            Optional<Entity> otherChair = getChairAtBlock(chair.get().getLocation());
            if (otherChair.isPresent()) {
                // someone else is already sitting here
                removeChair(chair.get(), true);
                return;
            }
            if (!isValid(chair.get().getLocation().getRelative(Direction.UP), false)) {
                // location is no longer valid place to sit
                removeChair(chair.get(), true);
                return;
            }
            chairLocations.put(chair.get().getUniqueId(), chair.get().getLocation());
            if (chair.get().get(CraftBookKeys.HEALING_CHAIR).orElse(false)) {
                chairHealingPlayers.add(event.getTargetEntity().getUniqueId()); // player UUID
            }
        }
    }

    // add any chairs loaded in from chunks
    // remove them if not in use
    @Listener
    @Include(SpawnEntityEvent.ChunkLoad.class)
    public void onChairLoad(SpawnEntityEvent event) {
        for (Entity chair : event.getEntities()) {
            if (chair.get(CraftBookKeys.CHAIR).orElse(false)) {
                // only care if players are on the chair?
                Optional<Entity> player = chair.getPassengers().stream().filter(passenger -> passenger instanceof Player).findAny();
                if (player.isPresent()) {
                    cooldowns.put(player.get().getUniqueId(), System.currentTimeMillis() / 1000L);
                    chairLocations.put(chair.getUniqueId(), chair.getLocation());
                    if (chair.get(CraftBookKeys.HEALING_CHAIR).orElse(false)) {
                        chairHealingPlayers.add(player.get().getUniqueId());
                    }
                } else {
                    // chair has no player
                    removeChair(chair, true);
                }
            }
        }
    }

    // Stop players without correct permission to place certain signs
    @Listener
    public void onChangeSignEvent(ChangeSignEvent e, @First Player player) {
        Sign sign = e.getTargetTile();
        Location<World> signLoc = sign.getLocation();
        BlockState chairBlock = signLoc.add(signLoc.require(Keys.DIRECTION).getOpposite().asBlockOffset()).getBlock();
        if (!isAllowed(chairBlock)) {
            return;
        }
        SignData data = e.getText();
        for (CustomChairType chairType : customChairTypes) {
            if (chairType.matches(data)) {
                if (!chairType.hasPermission(player)) {
                    player.sendMessage(Text.of(TextColors.RED, "You do not have permission to place a " + chairType.getName() + " sign"));
                    e.setCancelled(true);
                } else {
                    player.sendMessage(Text.of(TextColors.GREEN, "Successfully created " + chairType.getName()));
                }
                break;
            }
        }
    }

    public static Entity createChair(Location<World> location, Location<World> exitLocation) {
        Entity chair = location.getExtent().createEntity(EntityTypes.ARMOR_STAND, location.getPosition());
        chair.offer(Keys.INVISIBLE, true);
        chair.offer(Keys.HAS_GRAVITY, false);
        // prevents the armor stand entity from having a hitbox
        // which means it can not be pushed by pistons, other entities (shulkers, sweeping edge), or explosions
        // and does not prevent players from placing blocks at the location
        chair.offer(Keys.ARMOR_STAND_MARKER, true);
        // Incase somehow craftbook doesn't clean up, a quick /kill @e[type=armor_stand,name=CraftbookChair] would kill all chairs.
        chair.offer(Keys.DISPLAY_NAME, Text.of("CraftbookChair"));
        chair.offer(new ChairData(true));
        chair.offer(new ChairExitLocationData(exitLocation.getPosition()));
        return chair;
    }

    private static List<SpongeBlockFilter> getDefaultBlocks() {
        // All stairs facing north,east,south,west
        List<SpongeBlockFilter> states = Lists.newArrayList();
        Sponge.getRegistry().getAllOf(BlockType.class).stream()
                .filter(blockType -> blockType.getName().toLowerCase().contains("stairs"))
                .forEach(blockType -> {
                    for (Direction horizontalDir : BlockUtil.getDirectHorizontalFaces()) {
                        states.add(new SpongeBlockFilter(blockType.getName() + "[" + horizontalDir.toString() + "]"));
                    }
                });
        return states;
    }

    /**
     * WARNING: USE INSTEAD OF {@link BlockUtil}'s methods
     * This checks via BlockType and checks if it is upside-down, allowing
     * different facing stairs and corner stairs etc.
     *
     * @param state to check
     * @return if it is a valid chair block
     */
    private boolean isAllowed(BlockState state) {
        for (SpongeBlockFilter filter : allowedBlocks.getValue()) {
            for (BlockState filterState : filter.getApplicableBlocks()) {
                if (state.getType().equals(filterState.getType())) {
                    if (state.supports(Keys.PORTION_TYPE)) {
                        // Dont allow upside down
                        return state.require(Keys.PORTION_TYPE).equals(PortionTypes.BOTTOM);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String getPath() {
        return "mechanics/chairs";
    }

    @Override
    public ConfigValue<?>[] getConfigurationNodes() {
        return new ConfigValue[]{
                allowedBlocks,
                exitAtEntry,
                requireSigns,
                maxSignDistance,
                faceCorrectDirection,
                healAmount
        };
    }

    @Override
    public PermissionNode[] getPermissionNodes() {
        return new PermissionNode[]{
                useSitCommandPermission,
                toggleClickSitPermission,
                clickSitPermission
        };
    }

    public static class Sit implements CommandExecutor {
        private final Chairs instance;

        Sit(Chairs mechanicInstance) {
            instance = mechanicInstance;
        }

        @NonNull
        @Override
        public CommandResult execute(@NonNull CommandSource src, @NonNull CommandContext args) throws CommandException {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("Non-player cannot use this command!"));
            }
            Player player = (Player) src;
            if (player.getVehicle().isPresent()) {
                if (player.getVehicle().get().get(CraftBookKeys.CHAIR).orElse(false)) {
                    throw new CommandException(Text.of("You are already sitting."));
                } else {
                    throw new CommandException(Text.of("You must not be riding anything to use /sit."));
                }
            }
            Location<World> location = player.getLocation();
            // Prevent them negating fall damage or some other stupid shit by checking if they are on the ground.
            if (!player.isOnGround()) {
                throw new CommandException(Text.of("You must be on the ground to use /sit."));
            }
            // Create chair location.
            Location<World> chairLocation = location.sub(0, 0.2, 0);
            // prevent use of /sit in the ground
            if (!instance.isValid(chairLocation.getRelative(Direction.UP), false)) {
                throw new CommandException(Text.of("You can not sit here."));
            }

            if (instance.getChairAtBlock(chairLocation).isPresent()) {
                throw new CommandException(Text.of("Someone is already sitting here!"));
            }

            long unixTimeNow = System.currentTimeMillis() / 1000L;
            if (instance.cooldowns.containsKey(player.getUniqueId())) {
                Long duration = unixTimeNow - instance.cooldowns.get(player.getUniqueId());
                if (duration <= 3) {
                    throw new CommandException(Text.of("You can not sit yet."));
                }
            }
            instance.cooldowns.put(player.getUniqueId(), unixTimeNow);

            Entity chair = createChair(chairLocation, player.getLocation());

            // sit the same way they were facing before
            chair.setRotation(player.getRotation());

            Sponge.getCauseStackManager().pushCause(player);
            // Incase some protection plugin cancels it.
            if (!location.getExtent().spawnEntity(chair)) {
                throw new CommandException(Text.of("Failed to create a sitable seat for you."));
            }
            Sponge.getCauseStackManager().popCause();

            if (!chair.addPassenger(player) /*|| !entity.hasPassenger(player)*/) {
                chair.remove();
                throw new CommandException(Text.of("Failed to sit you down."));
            }
            player.sendMessage(Text.of(TextColors.YELLOW, "You sit down"));

            instance.chairLocations.put(chair.getUniqueId(), chairLocation);

            return CommandResult.success();
        }
    }

    public static class Stand implements CommandExecutor {
        private final Chairs instance;

        Stand(Chairs mechanicInstance) {
            instance = mechanicInstance;
        }

        @NonNull
        @Override
        public CommandResult execute(@NonNull CommandSource src, @NonNull CommandContext args) throws CommandException {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("Non-player cannot use this command!"));
            }
            Player player = (Player) src;
            Optional<Entity> chair = player.getVehicle();
            if (!chair.isPresent() || !chair.get().get(CraftBookKeys.CHAIR).orElse(false)) {
                throw new CommandException(Text.of("You are not sitting on a chair!"));
            }
            instance.removeChair(chair.get(), true);
            return CommandResult.success();
        }
    }

    /**
     * Command that allows players to stop right-click sit feature.
     */
    public static class ToggleClickSit implements CommandExecutor {
        private final Chairs instance;

        public ToggleClickSit(Chairs mechanicInstance) {
            instance = mechanicInstance;
        }

        @NonNull
        @Override
        public CommandResult execute(@NonNull CommandSource src, @NonNull CommandContext args) throws CommandException {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("Only players can use this command."));
            }
            Player player = (Player) src;
            boolean was = player.hasPermission(instance.clickSitPermission.getNode());
            player.getSubjectData().setPermission(SubjectData.GLOBAL_CONTEXT, instance.clickSitPermission.getNode(), was ? Tristate.FALSE : Tristate.TRUE);

            if (was) {
                src.sendMessage(Text.of(TextColors.GREEN, "You have now disabled sitting by clicking for yourself. Use to command again to return your ability to sit-click."));
            }
            else {
                src.sendMessage(Text.of(TextColors.GREEN, "You can now sit-click again."));
            }
            return CommandResult.success();
        }
    }

    static abstract class CustomChairType {
        /**
         * The permission to be checked when this sign is placed on a chair
         */
        private final SpongePermissionNode placePermission;
        protected final Chairs chairsInstance;

        CustomChairType(Chairs chairsInstance, SpongePermissionNode placePermission) {
            this.chairsInstance = chairsInstance;
            this.placePermission = placePermission;
        }

        public abstract void processChair(Entity chair);

        public abstract void processPlayerSit(Player player);

        /**
         * Returns true if the sign passed in signifies a chair of this sign
         *
         * @param signData of the sign that was placed
         * @return whether it is this type of chair
         */
        public abstract boolean matches(SignData signData);

        /**
         * Name of chair to be displayed when placed
         *
         * @return String name of type
         */
        public abstract String getName();

        /**
         * Checks if player has permission to place this sign
         * Can be overridden for no permission etc.
         *
         * @param player to check
         * @return true if they have permission
         */
        public boolean hasPermission(Player player) {
            return placePermission.hasPermission(player);
        }
    }

    static class HealingChairType extends CustomChairType {

        HealingChairType(Chairs chairsInstance, SpongePermissionNode placePermission) {
            super(chairsInstance, placePermission);
        }

        @Override
        public void processChair(Entity chair) {
            chair.offer(new ChairHealData(true));
        }

        @Override
        public void processPlayerSit(Player player) {
            this.chairsInstance.chairHealingPlayers.add(player.getUniqueId());
        }

        public boolean matches(SignData signData) {
            return SignUtil.getTextRaw(signData, 1).toLowerCase().equals("[sit heal]");
        }

        public String getName() {
            return "Sit Heal";
        }
    }
}
