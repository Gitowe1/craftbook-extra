/*
 * CraftBook Copyright (C) 2010-2025 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2025 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.sk89q.craftbook.sponge.mechanics.ics.chips.logic;

import com.sk89q.craftbook.sponge.CraftBookPlugin;
import com.sk89q.craftbook.sponge.ICFactory;
import com.minecraftonline.ic.AbstractIC;
import com.sk89q.craftbook.sponge.InvalidICException;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.scheduler.Task;

import java.util.List;
import java.util.UUID;

public class Repeater extends AbstractIC {

    private int delay = 0;
    private boolean delaySet = false;
    private boolean delayType = false;
    private Task delayTask;

    public Repeater(ICFactory<Repeater> icFactory, Location<World> block) {
        super(icFactory, block);
    }

    @Override
    public void create(Player player, List<Text> lines) throws InvalidICException {
        super.create(player, lines);

        String lineThree = lines.get(2).toPlain();
        if (!lineThree.isEmpty()) {
            String[] parts = lineThree.split(":");

            if (parts.length > 2) {
                throw new InvalidICException("Specify the delay as an integer (and optionally if the delay is in seconds) Delay(:S/Seconds)");
            } else if (parts.length > 1 && parts[0].isEmpty()) {
                throw new InvalidICException("You must specify the amount of delay if the delay type is set");
            }

            if (!parts[0].isEmpty()) {
                try {
                    delay = Integer.parseInt(parts[0]);
                    delaySet = true;
                } catch (NumberFormatException e) {
                    throw new InvalidICException("Invalid delay value. Please specify a valid integer.");
                }
            }

            if (delay < 0) {
                throw new InvalidICException("The delay cannot be less than zero.");
            }

            if (parts.length == 2) {
                if (parts[1].equalsIgnoreCase("S") || parts[1].equalsIgnoreCase("Seconds")) {
                    delayType = true;
                    if (delay > 5) {
                        throw new InvalidICException("The delay value cannot be more than 5 seconds.");
                    }
                } else {
                    throw new InvalidICException("Invalid delay type. Use 'S' or 'Seconds' to indicate delay in seconds.");
                }
            } else {
                if (delay > 100) {
                    throw new InvalidICException("The delay value cannot be more than 100 ticks.");
                }
            }
        }
    }

    @Override
    public void load() {
        super.load();

        String lineThree = getLine(2);
        if (!lineThree.isEmpty()) {
            String[] parts = lineThree.split(":");

            if (parts.length >= 1 && !parts[0].isEmpty()) {
                try {
                    delay = Integer.parseInt(parts[0]);
                    delaySet = true;
                } catch (NumberFormatException e) {
                    delaySet = false;
                }
            }

            if (parts.length == 2) {
                if (parts[1].equalsIgnoreCase("S") || parts[1].equalsIgnoreCase("Seconds")) {
                    delayType = true;
                }
            }
        }
    }

    @Override
    public void unload() {
        super.unload();
        if (delayTask != null) {
            delayTask.cancel();
        }
    }

    @Override
    public void onTrigger() {
        if (delaySet && delay >= 0) {
            delayTask = Task.builder()
                    .delayTicks(delayType ? delay * 20L : delay)
                    .execute(() -> getPinSet().setOutput(0, getPinSet().isAnyTriggered(this), this))
                    .name("Repeater Delay - " + UUID.randomUUID())
                    .submit(CraftBookPlugin.spongeInst());
        }
        else {
            CraftBookPlugin.spongeInst().getLogger().debug("Repeater at: " + getBlock());
            getPinSet().setOutput(0, getPinSet().isAnyTriggered(this), this);
        }
    }

    public static class Factory implements ICFactory<Repeater> {

        @Override
        public Repeater createInstance(Location<World> location) {
            return new Repeater(this, location);
        }

        @Override
        public String[][] getPinHelp() {
            return new String[][] {
                    new String[] {
                            "Input"
                    },
                    new String[] {
                            "Outputs input"
                    }
            };
        }
    }
}
